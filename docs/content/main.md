---
weight: 10
title: CSAT - CS-AWARE Translation Services
---

# Introduction

Graphene is the API responsible for information extraction in the [CS-AWARE Framework](https://cs-aware.eu). This service extracts knowledge graphs from texts (n-ary relations and rhetorical structures extracted from complex factoid discourse). Given a sentence or a text, it outputs a semantic representation of the text which is a labelled directed graph (a knowledge graph).

 
# Information Extraction

To translate cyber-securety-related text:

`POST http://<host>:8173/extract/`

```python
import requests

text = "Metamorphic computer virus written in assembly language for Microsoft Windows"

r = requests.post("http://<host>:8173/extract/", {"text" : text})

print(r.status_code)
print(r.headers)
print(r.content)
```

```shell
curl -d '{"text":"Metamorphic computer virus written in assembly language for Microsoft Windows"}' -H "Content-Type: application/json" -X POST http://<host>:8173/extract/
```

> The above command returns a json like this:

```json
{"name":"metamorphic computer virus written in assembly language for microsoft windows","core":
{"pureTerm":"virus","effectiveTerm":"virus","tag":"NN","specTypes":["spec","spec","written in-VBN"],
"specContents":[{"pureTerm":"computer","effectiveTerm":"computer","tag":"NN","specTypes":null,
"specContents":null,"planSpecs":[]},"pureTerm":"metamorphic","effectiveTerm":"metamorphic","tag":"JJ",
"specTypes":null,"specContents":null,"planSpecs":[]},{"pureTerm":"language","effectiveTerm":"language","tag":
"NN","specTypes":["spec","for-IN"],"specContents":[{"pureTerm":"assembly","effectiveTerm":"assembly","tag":"NN",
"specTypes":null,"specContents":null,"planSpecs":[]},{"pureTerm":"windows","effectiveTerm":"windows","tag":"NNS",
"specTypes":["spec"],"specContents":[{"pureTerm":"microsoft","effectiveTerm":"microsoft","tag":"JJ","specTypes":null,
"specContents":null,"planSpecs":[]}],"planSpecs":[["microsoft","spec"]]}],"planSpecs":[["assembly","spec"],
["microsoft","spec"],["windows","for-IN"]]}],"planSpecs":[["metamorphic","spec"],["computer","spec"],
["assembly","spec"],["microsoft","spec"],["windows","for-IN"],["language","written in-VBN"]]},"cores":["virus"]}
```

## Query Parameters

Path parameter | Mandatory | Description
--------- | ------- | -----------
text | yes | The text from which the knowledge graph will be extracted.

<aside class="success">
Remember — a successful execution returns HTTP code 200.
</aside>




