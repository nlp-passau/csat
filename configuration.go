package main

import (
	"path/filepath"
	"os"
	"os/user"
	"io/ioutil"
	"encoding/json"
	"log"
)

const DefaultDir = "/usr/local/csawere"
const ConfigFile = "csat.config"

type Config struct {
	Port int `json:"port,omitempty"`
	Translator struct {
		Provider string `json:"provider,omitempty"`
		Params map[string]string `json:"params,omitempty"`
	} `json:"translator,omitempty"`
	Messenger struct {
		ContentDir string `json:"contentDir,omitempty"`
	} `json:"messenger,omitempty"`
}

func getConfigFile() string {
	var candidates []string

	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	candidates = append(candidates, filepath.Join(dir, ConfigFile))
	candidates = append(candidates, filepath.Join(DefaultDir, ConfigFile))

	usr, err := user.Current()
	if err == nil {
		candidates = append(candidates, filepath.Join(usr.HomeDir, ConfigFile))
	}

	for _, c := range candidates {
		if _, err := os.Stat(c); err == nil {
			return c
		}
	}
	return ""
}

func GetConfiguration() *Config {
	filename := getConfigFile()
	if len(filename) > 0 {
		file, e := ioutil.ReadFile(filename)
		if e != nil {
			log.Fatal("File error: %v\n", e)
			os.Exit(-1)
		}

		var config Config
		json.Unmarshal(file, &config)
		return &config

	} else {
		return nil
	}
}
