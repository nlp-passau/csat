package translation

import (
	gt "cloud.google.com/go/translate"
	"context"
	"golang.org/x/text/language"
	"google.golang.org/api/option"
)

type GoogleTranslator struct {
	service *gt.Client
	ctx context.Context
	opts option.ClientOption
	transOpts *gt.Options
}

func NewGoogleTranslator(Params map[string]string) *GoogleTranslator {
	//TODO define the context and options from the params
	var ctx context.Context = nil
	var opts option.ClientOption = nil
	service, err := gt.NewClient(ctx, opts)
	if err != nil {
		//do something here
	}

	transOpts := &gt.Options{language.Make("en"), gt.Text, "nmt"}
	return &GoogleTranslator{service, ctx, opts, transOpts}
}

func (t GoogleTranslator) Translate(text string, from string, to string) (string, error) {
	translations, err := t.service.Translate(t.ctx, []string{text}, language.Make(to), t.transOpts)

	if err != nil {
		return "", err
	}
	return translations[0].Text, nil
}

func (t GoogleTranslator) Close() error {
	return t.service.Close()
}

func (t GoogleTranslator) About() (string, string) {
	return "Google Translator", "v. 0.0.1"
}