package translation

type MockTranslator struct {}

func (t MockTranslator) Translate(text string, from string, to string) (result string, nil error) {
	for _,v := range text {
		result = string(v) + result
	}
	return
}

func (t MockTranslator) Close() error {
	return nil
}

func (t MockTranslator) About() (string, string) {
	return "Mock Translator", "v.0.0.1"
}
