package translation

import (
	"os"
	"log"
	"bufio"
	"path/filepath"
	"strings"
	"strconv"
	"errors"
	"fmt"
)

const SEPARATOR = "="

type OurMessenger struct {
	baseDir string
	content map[string][]string
}

func NewMessenger(baseDir string) *OurMessenger {
	return &OurMessenger{baseDir:baseDir, content: make(map[string][]string)}
}

func (m OurMessenger) GetAll(lang string) ([]string, error) {
	f := filepath.Join(m.baseDir, lang)
	if _, err := os.Stat(f); err != nil {
		return nil, errors.New(fmt.Sprintf("lang=%s not available.", lang))
	}

	if content, ok := m.content[lang]; ok {
		return content, nil
	}

	file, err := os.Open(f)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var content []string

	scanner := bufio.NewScanner(file)
	counter := 0
	for scanner.Scan() {
		parts := strings.SplitN(scanner.Text(), SEPARATOR, 2)
		i, err := strconv.Atoi(parts[0])

		if err != nil || i != counter {
			return nil, errors.New("invalid file.")
		}

		counter++
		content = append(content, parts[1])
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	m.content[lang] = content
	return content, nil
}

func (m OurMessenger) GetMessage(code int, lang string) (string, error) {
	content, err := m.GetAll(lang)

	if err != nil {
		return "", err
	}
	if code < 0 || code >= len(content) {
		return "", errors.New(fmt.Sprintf("Invalid code. For lang=%s, codes ranges from %d to %d.", lang,0, len(content)-1))
	}

	return content[code], nil
}

func (m OurMessenger) About() (string, string) {
	return "Internal Multilingual Messenger", "v. 0.0.1"
}