package translation

type OpenNMTranslator struct {
	url string
}

func NewOpenNMTranslator(Params map[string]string) *OpenNMTranslator {
	return &OpenNMTranslator{Params["url"]}
}

func (t OpenNMTranslator) Translate(text string, from string, to string) (result string, nil error) {
	// TODO https://github.com/OpenNMT/OpenNMT-tf/blob/master/examples/serving/ende_client.py
	return
}

func (t OpenNMTranslator) Close() error {
	return nil
}

func (t OpenNMTranslator) About() (string, string) {
	return "OpenNMT Translator - trained with the EuroParl corpus v7.0", "v.0.0.1"
}
