package translation

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/url"
)

type BingTranslator struct {
	subscriptionKey string
}

type TranslationResult struct {
	DetectedLanguage struct {
		Language string `json:"language,omitempty"`
		Score int `json:"score,omitempty"`
	} `json:"detectedLanguage,omitempty"`
	Translations []struct {
		Text string `json:"text,omitempty"`
		To string `json:"to,omitempty"`
	} `json:"translations,omitempty"`
}


func NewBingTranslator(Params map[string]string) *BingTranslator {
	return &BingTranslator{Params["subscriptionKey"]}
}

func (t BingTranslator) Translate(text string, from string, to string) (string, error) {
	u, _ := url.Parse("https://api.cognitive.microsofttranslator.com/translate?api-version=3.0")
	q := u.Query()
	q.Add("to", to)
	u.RawQuery = q.Encode()

	body := []struct {
		Text string
	}{
		{Text: text},
	}
	b, _ := json.Marshal(body)

	// Build the HTTP POST request
	req, err := http.NewRequest("POST", u.String(), bytes.NewBuffer(b))
	if err != nil {
		log.Fatal(err)
	}
	// Add required headers to the request
	req.Header.Add("Ocp-Apim-Subscription-Key", t.subscriptionKey)
	req.Header.Add("Content-Type", "application/json")

	// Call the Translator Text API
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	// Decode the JSON response
	var result []TranslationResult
	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		log.Fatal(err)
	}

	return result[0].Translations[0].Text, nil
}

func (t BingTranslator) Close() error {
	return nil
}

func (t BingTranslator) About() (string, string) {
	return "Bing Translator", "v. 0.0.1"
}