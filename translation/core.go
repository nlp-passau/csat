package translation

type Translator interface {
	Translate(text string, from string, to string) (string, error)
	About() (string, string)
	Close() error
}

type MultilingualInterfaceMessenger interface {
	GetAll(lang string) ([]string, error)
	GetMessage(code int, lang string) (string, error)
	About() (string, string)
}
