package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/nlp-passau/csat/translation"
	"log"
	"net/http"
	"strconv"
)

func loadServices(config *Config) *Services {
	messenger := translation.NewMessenger(config.Messenger.ContentDir)
	var translator translation.Translator

	switch config.Translator.Provider {
	case "google":
		translator = translation.NewGoogleTranslator(config.Translator.Params)
		log.Print("Google Translator selected.")
	case "bing":
		translator = translation.NewBingTranslator(config.Translator.Params)
		log.Print("Bing Translator selected.")
	case "internal":
		translator = translation.NewOpenNMTranslator(config.Translator.Params)
		log.Print("Internal (OpenNMT) Translator selected.")
	default:
		translator = translation.MockTranslator{}
		log.Printf("'%s' translator not defined.", config.Translator.Provider)
		log.Print("Mock Translator selected.")
	}

	return &Services{translator:translator, messenger: messenger}
}

type Services struct {
	translator translation.Translator
	messenger translation.MultilingualInterfaceMessenger
}

func (s *Services) Help(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n\n - to translate a text:\n")
	fmt.Fprint(w," GET /translate/{text}/{from}/{to}\n")
	fmt.Fprint(w,"    Examples of use:\n")
	fmt.Fprint(w,"    - GET /translate/love/en/it\n")
	fmt.Fprint(w,"    - GET /translate/Arrivederci/it/en\n\n")

	fmt.Fprint(w, " - to get an interface message :\n")
	fmt.Fprint(w,"    - GET /interface/message/{id}/{lang}\n")
	fmt.Fprint(w,"    Examples of use:\n")
	fmt.Fprint(w,"    - GET /interface/message/1/it\n")
	fmt.Fprint(w,"    - GET /interface/message/3/el\n\n")

	fmt.Fprint(w, " - to get all interface messages :\n")
	fmt.Fprint(w,"    - GET /interface/all/{lang}\n")
	fmt.Fprint(w,"    Examples of use:\n")
	fmt.Fprint(w,"    - GET /interface/all/it\n")
	fmt.Fprint(w,"    - GET /interface/all/el\n\n")

	fmt.Fprint(w, " - about the services:\n")
	fmt.Fprint(w,"    - GET /interface/about\n")
	fmt.Fprint(w,"    - GET /translate/about\n")

}

func (s *Services) GetAll(w http.ResponseWriter, r *http.Request) {
	l := mux.Vars(r)["lang"]
	log.Printf("[GetAll] for lang %s", l)

	data, err := s.messenger.GetAll(l)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(data)
	}
}

func (s *Services) GetMessage(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	l := mux.Vars(r)["lang"]
	log.Printf("[GetMessage] for id=%s and lang=%s", id, l)

	i, err := strconv.Atoi(id)

	if err != nil {
		m := fmt.Sprintf("id should be a positive interger (id=%s).", id)
		http.Error(w, m, http.StatusBadRequest)
		log.Print(m)
	} else {
		w.Header().Set("Content-Type", "text/plain'")
		m, err := s.messenger.GetMessage(i, l)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			log.Print(err.Error())
		}
		w.Write([]byte(m))
	}
}

func (s *Services) GetAboutMessenger(w http.ResponseWriter, r *http.Request) {
	var d, v = s.messenger.About()
	w.Header().Set("Content-Type", "text/plain'")
	fmt.Fprintf(w, "%s - %s", d, v)
}

func (s *Services) Translate(w http.ResponseWriter, r *http.Request) {
	trans, err := s.translator.Translate(mux.Vars(r)["text"], mux.Vars(r)["from"], mux.Vars(r)["to"])

	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		log.Print(err.Error())
	} else {
		w.Header().Set("Content-Type", "text/plain'")
		fmt.Fprint(w, trans)
	}
}

func (s *Services) GetAboutTranslator(w http.ResponseWriter, r *http.Request) {
	var d, v = s.translator.About()
	w.Header().Set("Content-Type", "text/plain'")
	fmt.Fprintf(w, "%s - %s", d, v)
}
