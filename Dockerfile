FROM golang

ADD . /go/src/gitlab.com/nlp-passau/csat
RUN go get gitlab.com/nlp-passau/csat
RUN go install gitlab.com/nlp-passau/csat
ENTRYPOINT /go/bin/csat

RUN mkdir /usr/local/csawere
RUN echo "{\"port\":8080, \"messenger\" : {\"contentDir\" : \"/go/src/gitlab.com/nlp-passau/csat/test/i18n\"}}" > /usr/local/csawere/csat.config

EXPOSE 8080
