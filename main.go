package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"fmt"
)

func main() {
	config := GetConfiguration()

	if config != nil {
		log.Printf("Running csat service in port %d", config.Port)
		services := loadServices(config)

		router := mux.NewRouter()
		router.HandleFunc("/", services.Help).Methods("GET")
		router.HandleFunc("/interface/all/{lang}", services.GetAll).Methods("GET")
		router.HandleFunc("/interface/message/{id}/{lang}", services.GetMessage).Methods("GET")
		router.HandleFunc("/interface/about", services.GetAboutMessenger).Methods("GET")

		router.HandleFunc("/translate/{text}/{from}/{to}", services.Translate).Methods("GET")
		router.HandleFunc("/translate/about", services.GetAboutTranslator).Methods("GET")

		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), router))
	} else {
		log.Fatal("No configuration file found.")
	}
}
